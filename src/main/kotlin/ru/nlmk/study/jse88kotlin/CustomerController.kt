package ru.nlmk.study.jse88kotlin

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.nlmk.study.jse88kotlin.model.Customer
import ru.nlmk.study.jse88kotlin.repository.CustomerRepository

@RestController
@RequestMapping("/customer")
class CustomerController(private val customerRepository: CustomerRepository) {

    @GetMapping("/all")
    fun getAll(): List<Customer> = customerRepository.findAll()
}