package ru.nlmk.study.jse88kotlin.model

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "customer")
class Customer(
    @Id
    val id: Long,
    val firstName: String,
    val secondName: String
)