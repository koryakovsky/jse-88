package ru.nlmk.study.jse88kotlin.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.nlmk.study.jse88kotlin.model.Customer

@Repository
interface CustomerRepository : JpaRepository<Customer, Long>